#include "threads.h"

//Print I Love threads
void I_Love_Threads()
{
	cout << "I Love threads" << endl;
}

//Create thread and call the function I_Love_Threads
void call_I_Love_Threads()
{
	thread first(I_Love_Threads);
	first.join();
}

//Print The vector until size not equal to  0 
void printVector(vector<int> primes)
{
	if (primes.size() > 0)
	{
		cout << primes[(int)primes.size() - 1] << endl;
	}
}

//Check prime numbers and push the values in the vector  
void getPrimes(int begin, int end, vector<int>& primes)
{
	bool isPrimeNum = true;

	for (int i = begin; i < end; i++)
	{
		printVector(primes);
		for (int j = 2; j < i / 2; j++)
		{
			if (i % j == 0)
			{
				isPrimeNum = false; 
				break;
			}
		}
		if (isPrimeNum)
		{
			primes.push_back(i);
		}
		isPrimeNum = true; 
	}
}

//create thread and the vector 
vector<int> callGetPrimes(int begin, int end)
{
	vector<int> primes; 
	thread first(getPrimes, begin, end, ref(primes));
	first.join();
	return primes;
}

//Check prime numbers and write to the file the prime number
void writePrimesToFile(int begin, int end, ofstream & file)
{
	bool isPrimeNum = true;
	for (int i = begin; i < end; i++)
	{
		for (int j = 2; j < i / 2; j++)
		{
			if (i % j == 0)
			{
				isPrimeNum = false;
				break;
			}
		}
		if (isPrimeNum)
		{
			file << i << "\n";
		}
		isPrimeNum = true;
	}
}

//Open the file , create a thread  n iterations
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	ofstream file; 
	file.open(filePath);
	thread first(writePrimesToFile, 0, N, ref(file));
	first.detach();
	for (int i = begin; i < end; i++)
	{
		if (i%N == 0)
		{
			thread first(writePrimesToFile, i, i + N, ref(file));
			first.detach();
		}
	}
}
