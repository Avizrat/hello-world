#include "threads.h"



int main()
{

	//Ex1
	call_I_Love_Threads();

	//Ex2
	vector<int> primes1;
	getPrimes(58, 100, primes1);

	//Ex3
	vector<int> primes1000 = callGetPrimes(0, 1000);
    vector<int> primes100000 = callGetPrimes(0, 100000);
	vector<int> primes1000000 = callGetPrimes(0, 1000000);


	//Ex4 , 5 
	callWritePrimesMultipleThreads(1, 100000, "primes2.txt", 2);

	system("pause");
	return 0;
}