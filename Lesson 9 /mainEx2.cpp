#include "functions.h"
#include <iostream>
#include "Object.h"

int main() {

	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;

	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<char>('a', 'd') << std::endl;
	std::cout << compare<char>('x', 'n') << std::endl;
	std::cout << compare<char>('s', 's') << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;

	Object arr[arr_size] = { Object(5), Object(16), Object(4),Object(55), Object(2) };
	bubbleSort<Object>(arr, arr_size);

	std::cout << "correct print is sorted array" << std::endl;
	printArray<Object>(arr, arr_size);
	std::cout << std::endl;

	system("pause");
	return 1;
}
