#include "BSNode.h"
#include <iostream> 
#include <math.h>
#include <algorithm>


using namespace std;


BSNode::BSNode(string data)
{
	this->_data = data;
	this->_left = nullptr;
	this->_right = nullptr;
	this->_count = 1; 
}

BSNode::BSNode(const BSNode & other)
{
	this->_data = other._data;
	this->_left = other._left;
	this->_right = other._right;
	this->_count = other._count;
}

BSNode::~BSNode()
{
		
}

void BSNode::insert(string value)
{ 
	if (value.compare(this->_data) < 0) { //If the value is smaller it goes to the left

		if (this->_left == nullptr) 
		{
			this->_left = new BSNode(value);
		}
		else
			return this->_left->insert(value);
	}
	else if (value.compare(this->_data) > 0) { //If the value is bigger it goes to the right
		if (this->_right == nullptr) {
			this->_right = new BSNode(value);
		}
		else
			return this->_right->insert(value);
	}
	else
		this->_count++;

}

BSNode & BSNode::operator=(const BSNode & other)
{
	this->_data = other._data;
	this->_left = other._left;
	this->_right = other._right;
	return *this;
}

bool BSNode::isLeaf() const
{
	if (this->_left == NULL && this->_right == NULL)
	{
		return true;
	}
		return false;
}

string BSNode::getData() const
{
	return this->_data;
}

BSNode * BSNode::getLeft() const
{
	return this->_left;
}

BSNode * BSNode::getRight() const
{
	return this->_right;
}

bool BSNode::search(string val) const
{
	if (this == nullptr)
	{
		return false;
	}
	if (this->_data == val)//Value found
	{
		return true;

	}
	if (this->_data.compare(val) < 0) //Searches to the left if the value is smaller, otherwise right
	{
		return this->_right->search(val);
	}
	return this->_left->search(val);

}

int BSNode::getHeight() const
{

	if (this == nullptr)
	{
		return 0; 
	}
	if(this->_left->getHeight() > this->_right->getHeight())
	{
		return 1 + this->_left->getHeight();
	}
	else if(this->_right->getHeight() > this->_left->getHeight())
	{
		return 1 + this->_right->getHeight();
	}
}

int BSNode::getDepth(const BSNode & root) const
{
	if (!root.search(this->_data))
	{
		return -1;
	}
	this->getCurrNodeDistFromInputNode(&root);
}

void BSNode::printNodes() const
{
	if (this == NULL)
	{
		return;
	}
	else
	{
		this->_left->printNodes();
		this->_right->printNodes();
		cout << this->_data << "" <<" "<< this->_count<<""<<"\n";
	}

}

int BSNode::getCurrNodeDistFromInputNode(const BSNode * node) const
{
	if (node == nullptr)
	{
		return 0;
	}
	if (node->getData() == this->getData())
	{
		return 1;
	}
	return 1 + std::max(this->getCurrNodeDistFromInputNode(node->getLeft()), this->getCurrNodeDistFromInputNode(node->getRight()));
}
