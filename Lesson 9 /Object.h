#pragma once
class Object
{
public: int num;
		Object(int num);
		Object();

		~Object();

		bool operator==(const Object& other);
		void operator=(const Object& other);
		bool operator>(const Object& other);
		friend std::ostream& operator<<(std::ostream& os, const Object obj);
};
