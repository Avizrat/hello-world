#include "Object.h"
#include <iostream>

Object::Object(int num)
{
	this->num = num;
}

Object::Object()
{
	this->num = num;
}

Object::~Object()
{

}

//checks if equal
bool Object::operator==(const Object& other)
{
	return this->num == other.num;
}

//copy value
void Object::operator=(const Object& other)
{
	this->num = other.num;
}

//checks if value is bigger
bool Object::operator>(const Object& other)
{
	return (this->num > other.num);
}

std::ostream& operator<<(std::ostream& os, const Object obj)
{
	os << obj.num;
	return os;
}
