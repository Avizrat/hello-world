#include "BSNodeEx3.h"
#include <iostream>

#define ARR_SIZE 15

int main()

{
	int i;
	int arrInt[ARR_SIZE] = { 6,5,3,8,2,6,7,1,9,0,5,12,34,4,11 };
	BSNode<int>* treeInt = new BSNode<int>(arrInt[0]);
	for (i = 0; i < ARR_SIZE; i++)
	{
		std::cout << arrInt[i] << " ";
		treeInt->insert(arrInt[i]);
	}
	std::cout << endl;
	treeInt->printNodes();
	char arrChar[ARR_SIZE] = { 'f','h','w','a','c','d','s','g','z','x','w','k','u','b','!' };
	BSNode<char>* treeChar = new BSNode<char>(arrChar[0]);
	for (i = 0; i < ARR_SIZE; i++)
	{
		std::cout << arrChar[i] << " ";
		treeChar->insert(arrChar[i]);
	}
	treeChar->printNodes();
	getchar();
}

