#include "Customer.h"

Customer::Customer(string name) 
{
	this->_name = name;
	
}

Customer::Customer()
{
	this->_name = "NO_NAME";
}

double Customer::totalSum() const
{
	double sum = 0;

	set<Item> ::iterator itr;
	for (itr = _items.begin(); itr != _items.end(); ++itr)
	{
		sum += (*itr).totalPrice();
	}
	return sum;
}

void Customer::addItem(Item item)
{
	Item tempItemp(item);
	int thisCount = 0;
	string currentItemName = item.getName();
	string itrString = "";
	if (findItem(item.getName()))
	{
		set<Item> ::iterator itr;
		for (itr = _items.begin(); itr != _items.end(); ++itr)
		{
			itrString = (*itr).getName();
			int result = strcmp(itrString.c_str(), currentItemName.c_str()) == 0;
			if (result == 1)
			{
				thisCount = (*itr).getCount();

			}
			else
			{
				cout << "b";
			}
		}
		thisCount++;
		tempItemp.setCount(thisCount);
		_items.erase(item);
		_items.insert(tempItemp); // Set new count
		return;
	}
	_items.insert(item);
}

bool Customer::removeItem(Item item)
{
	int currentTempCount = 0; // Get current item count
	Item tempItemp(item);
	if (findItem(item.getName())) // Check if item exists
	{
		currentTempCount = getCount(item.getName());
		if (currentTempCount == 1) // If it is the last product  
		{
			_items.erase(item); // Erase him completly
			return true;
		} // Else
		tempItemp.setCount(currentTempCount - 1);
		_items.erase(item);
		_items.insert(tempItemp); // Set new count
		return true;
	}	
	cout << "ERROR " << item.getName() << " was not found !" << endl;
	return false;
}

bool Customer::findItem(string item)
{
	set<Item> ::iterator itr;
	for (itr = _items.begin(); itr != _items.end(); ++itr)
	{
		if ((*itr).getName() == item)
		{
			return true;
		}
	}
	return false;
}

int Customer::getCount(string item)
{
	set<Item> ::iterator itr;
	for (itr = _items.begin(); itr != _items.end(); ++itr)
	{
		if ((*itr).getName() == item)
		{
			return (*itr).getCount();
		}
	}
	return -1;
}

void Customer::printLine()
{
	for (int i = 0; i < 50; i++)
	{
		Sleep(3); // :)
		cout << "-";
	}cout << endl;
}

void Customer::setItem(const set<Item>& other)
{
	_items.clear(); // Delete previus items
	set<Item> ::iterator other_itr;
	for (other_itr = other.begin(); other_itr != other.end(); ++other_itr)
	{
		_items.insert(*(other_itr));
	}
}

void Customer::setName(string name)
{
	this->_name = name;
}

const set<Item> & Customer::getItems() 
{
	return _items;
}

string Customer::getNameOfCustumer()
{
	return this->_name;
}

void Customer::toString()
{
	set<Item> ::iterator itr;
	printLine();
	cout << "Customer:" << _name << endl;
	int counter = 1;
	for (itr = _items.begin(); itr != _items.end(); ++itr , counter++)
	{
		cout << counter << " ";
		(*itr).toString();
		Sleep(100);	
	}
	if (!--counter)
	{
		cout << "List is empty"<<endl;
		return;
	}
	cout << "Total sum is:" << totalSum() << endl;
	printLine();
}
