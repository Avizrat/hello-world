#include "Item.h"

Item::Item(string a, string b, double c)
{
	this->_name = a;
	this->_serialNumber = b;
	this->_unitPrice = c;
	this->_count = 1;
}

Item::Item(const Item & other)
{
	this->_name = other._name;
	this->_serialNumber = other._serialNumber;
	this->_unitPrice = other._unitPrice;
	this->_count = other._count;
}

Item::~Item()
{

}

string Item::getSerialNumber() const
{
	return _serialNumber;
}



string Item::getName() const
{
	return _name;
}

int Item::getCount() const
{
	return _count;
}

double Item::getUnitPrice() const
{
	return _unitPrice;
}

void Item::setSerialNumbers(string cNumber)
{
	this->_serialNumber = cNumber;
}

void Item::setName(string nName)
{
	this->_name = nName;
}

void Item::setCount(int nCounter)
{
	this->_count = nCounter;
}

void Item::setUnitPrice(double unitPrice)
{
	this->_unitPrice = unitPrice;
}

void Item::toString() const
{
	cout <<  getName() << " price:" << getUnitPrice() << " serial: " << getSerialNumber() << " amount:" << getCount() << endl;
}

void Item::toStringBasic() const
{
	cout << getName() << " price:" << getUnitPrice() << endl;
}

void Item::toStringClient() const
{
	cout << getName() << " price:" << getUnitPrice()  << " amount:" << getCount() << endl;
}

double Item::totalPrice() const
{
	return _count * _unitPrice;
}

bool Item::operator<(const Item & other) const
{
	return _serialNumber < other._serialNumber;
}

bool Item::operator>(const Item & other) const
{
	return _serialNumber > other._serialNumber;
}

bool Item::operator==(const Item & other) const
{
	return _serialNumber ==  other._serialNumber;
}

void Item::operator=(const Item & other)
{
}
