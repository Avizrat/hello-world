#include"Customer.h"
#include<map>

void menuOfMarket(map<string, Customer> & abcCustomers , Item itemList[]); // Run menu
bool isClientExist(string name, map<string, Customer> & abcCustomers); // Check if client exist
bool signCustomer(map<string, Customer> & abcCustomers , Item itemList[]); // Sign new customer
bool updateItemsList(map<string, Customer>& abcCustomers,string nameOfCustomer, Item itemList[] , bool addRem); // Option 2 (adding and removing)
void printCurrentItems(Item itemList[]); // Print list of items
void updateClient(map<string, Customer>& abcCustomers, Item itemList[]); // Adding and removing manager
void findWhoSpentTheMost(map<string, Customer>& abcCustomers); // Option 3...

void updateClient(map<string, Customer>& abcCustomers, Item itemList[])
{
	system("CLS");
	string clientName = "";
	cout << "Enter client's name --> ";
	cin >> clientName;
	getchar();
	if (!isClientExist(clientName, abcCustomers))
	{
		cout << "\nError " << clientName << " is not found" << endl;
		return;
	}
	int answer = 0;
	while (answer != 3)
	{
		system("CLS");
		abcCustomers.find(clientName)->second.toString();
		cout << "1.      Add items\n2.      Remove items\n3.      Back to menu\n--> ";
		cin >> answer;
		switch (answer)
		{
		case 1:
			updateItemsList(abcCustomers, clientName, itemList , true);
			break;
		case 2:
			updateItemsList(abcCustomers, clientName, itemList, false);
			break;
		default:
			break;
		}
	}
}

void findWhoSpentTheMost(map<string, Customer>& abcCustomers)
{
	system("CLS");
	map<string, Customer> ::iterator itr;
	int max = 0;
	int len = 0;
	string big = "";
	for (itr = abcCustomers.begin(); itr != abcCustomers.end(); ++itr , len++)
	{
		if (itr->second.totalSum() > max)
		{
			max = itr->second.totalSum();
			big = itr->first;
		}
	}
	if (len)
	{
		cout << "The guy who paid the most mouney on cart list is " << big << endl;
		cout << "This is his shoping list" << endl;
		abcCustomers.find(big)->second.toString();
		cout << endl;
	}
	else
	{
		cout << "\n No one is here!" << endl;
	}
	
}



void menuOfMarket(map<string, Customer> & abcCustomers , Item itemList[])
{
	string menu = "Welcome to MagshiMart!\n1.      to sign as customer and buy items\n2.      to update existing customers items\n3.      to print the customer who pays the most\n4.      to exit\n--> ";	
	int answer = 0;	
	while (answer != 4)
	{
		cout << menu;
		cin >> answer;
		getchar();
		switch (answer)
		{
		case 1:
			signCustomer(abcCustomers , itemList);
			break;
		case 2:
			updateClient(abcCustomers, itemList);
			break;
		case 3:
			findWhoSpentTheMost(abcCustomers);
			break;
		case 4:
			break;
		default:
			break;
		}
		getchar();  system("CLS");
	}
	abcCustomers.clear();
}

bool isClientExist(string name,map<string, Customer>& abcCustomers)
{
	map<string, Customer> ::iterator itr;
	for (itr = abcCustomers.begin(); itr != abcCustomers.end(); ++itr)
	{
		if (name == itr->first)
		{
			return true;
		}
	}
	return false;
}

bool signCustomer(map<string, Customer>& abcCustomers , Item itemList[])
{
	system("CLS");
	string clientName = "";
	cout << "Enter client's name --> "; 
	cin >> clientName;
	getchar();
	if (!isClientExist(clientName , abcCustomers))
	{
		std::pair<string, Customer> newCustomer;
		newCustomer.second = Customer(clientName);
		newCustomer.first = clientName;
		abcCustomers.insert(newCustomer);
		updateItemsList(abcCustomers, clientName, itemList,true);
		return true;
	}
	cout << "\nError " << clientName << " is already signed up"<<endl;
	
	return false;
}

bool updateItemsList(map<string, Customer>& abcCustomers, string nameOfCustomer , Item itemList[] , bool addRem)
{
	int answer = 1;
	system("CLS");
	while (answer != 0)
	{
		printCurrentItems(itemList);
		cin >> answer; getchar();
		if (answer > 0 && answer <= 10)
		{
			if (addRem)
				abcCustomers.find(nameOfCustomer)->second.addItem(itemList[answer - 1]);
			else
				if (!abcCustomers.find(nameOfCustomer)->second.removeItem(itemList[answer - 1]))
				{
					getchar();
				}
		}
		system("CLS");
	}
	return false;
}

void printCurrentItems(Item itemList[])
{
	cout << "The items you can buy are: (0 to exit)" << endl;
	for (int i = 0; i < 10; i++)
	{
		cout << i + 1 << " ";
		itemList[i].toStringBasic();
	}cout << "\n--> ";
}


int main()
{
	map<string, Customer> abcCustomers;
	Item itemList[10] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32)
	};
	menuOfMarket(abcCustomers, itemList);
	return 0;
}

