#pragma once
#include<iostream>
#include<string>
#include<algorithm>
#include "windows.h" 
using namespace std;

class Item
{
public:
	Item(string a , string b, double c);
	Item(const Item& other);
	~Item();
	
	double totalPrice() const; //returns _count*_unitPrice
	bool operator <(const Item& other) const; //compares the _serialNumber of those items.
	bool operator >(const Item& other) const; //compares the _serialNumber of those items.
	bool operator ==(const Item& other) const; //compares the _serialNumber of those items.
	void operator =(const Item& other);

	// Getters

	string getSerialNumber() const;
	string getName() const;
	int getCount() const;
	double getUnitPrice() const;

	// Setters

	void setSerialNumbers(string cNumber);
	void setName(string nName);
	void setCount(int nCounter);
	void setUnitPrice(double unitPrice);
	void toString() const; // Print current item /s 
	void toStringBasic() const; // Shows only price and prodact
	void toStringClient() const; // Show product price and amount

private:
	string _name;
	string _serialNumber; //consists of 5 numbers
	int _count; //default is 1, can never be less than 1!
	double _unitPrice; //always bigger than 0!

};