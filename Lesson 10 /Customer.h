#pragma once
#include"Item.h"
#include<set>

class Customer
{
public:
	Customer(string name);
	Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item item);//add item to the set
	bool removeItem(Item item);//remove item from the set

	//Getters
	const set<Item>& getItems(); // Return direct pointer of _items 
	string getNameOfCustumer();
								 // Setters 
	void setItem(const set<Item> &other); // Deep copy another list...
	void setName(string name);
	void toString(); // Print current custumer data


private:
	string _name;
	set<Item> _items;
	bool findItem(string item);
	int getCount(string item);
	void printLine();

};
