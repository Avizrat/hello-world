#pragma once

#include <WinSock2.h>
#include <deque>

using namespace std;

class Server
{
	SOCKET _socket;
	deque<string> _clients;
	string _document;

public:
	Server();
	~Server();

	void serveInit(int port) const;

	SOCKET getSocket() const;
	deque<string> getClients() const;

	string getDocument() const;
	void setDocument(string document);

	int locateClient(string client) const;
	bool isLoggedIn(string client) const;
	void addToQueue(string client);
	void removeFromQueue(string client);
	string progressQueue();

private:
	void accept() const;
};
