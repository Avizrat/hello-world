#include "WSAInitializer.h"

#include <WinSock2.h>
#include <Windows.h>
#include <exception>

WSAInitializer::WSAInitializer()
{
	WSADATA wsa_data = {};
	if (WSAStartup(MAKEWORD(2, 2), &wsa_data) != 0)
		throw std::exception(__FUNCTION__ " - WSAStartup Failed");
}

WSAInitializer::~WSAInitializer()
{
	try
	{
		WSACleanup();
	}
	catch (...) {}
}
