#pragma once

#include <vector>
#include "Server.h"

typedef struct SocketInfo
{
	Server* _server;
	SOCKET _socket;
} SocketInfo;

using namespace std;

class Client
{
private:
	string _name;
	Server* _server;
	SOCKET _socket;

	void communicate();
	void update();

	void loginRequest(); // 200
	void updateDocument(); // 204
	//void clientFinish(); // 207
public:
	Client(SocketInfo info);
	~Client();

	string getName() const;
	SOCKET getSocket() const;
};

static vector<Client*> clients;

void updateAll(Server* server, string ignore);
Client* newClient(SocketInfo info);
