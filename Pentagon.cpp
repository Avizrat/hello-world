#include "Pentagon.h"
#include "shapeexception.h"
#include "MathUtlis.h"



Pentagon::Pentagon(std::string col, std::string nam, double mem) :Shape(col, nam)
{
	if (mem < 0)
		throw shapeException();
	setMember(mem);
}


Pentagon::~Pentagon()
{
}

void Pentagon::draw()
{
	std::cout << getName() << std::endl << getColor() << std::endl << "Area is " << calArea() << std::endl;
}
double Pentagon::calArea() const
{
	return MathUtlis::calPentagonArea(member);
}
void Pentagon::setMember(double member)
{
	if (member < 0)
		throw shapeException();
	this->member = member;
}