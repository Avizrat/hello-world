#include "MessagesSender.h"


using namespace std;
MessagesSender::MessagesSender()
{
}

MessagesSender::~MessagesSender()
{
}

void MessagesSender::main()
{
	int choice = 0;

	cout << "1.\tsignIn\n2.\tsignOut\n3.\tConnected Users\n4.\texit\n" << endl;
	cout << "Enter Your Choice:" << endl;
	cin >> choice;
	while (choice != 4) // exit
	{
		switch (choice)
		{
		case 1: // signIn fuction
			signIn();
			break;
		case 2:
			signOut();// signIn fuction
		case 3:
			connectedUsers();// connectedUsers fuction 
		default:
			break;
		}
		system("Pause");
	}
	system("Pause");
}

void MessagesSender::signIn()
{
	string user = "";
	cout << "Enter Your Username:" << endl;
	cin >> user;
	unique_lock<mutex> lock(_mt);

	if (_usersConnected.find(user) != _usersConnected.end())
	{
			cout << "Already taken" << endl;
	}
	else
	{
			_usersConnected.insert(user);
	}

	lock.unlock();
}

void MessagesSender::signOut()
{
	string user = "";
	cout << "Enter Your Username to log out:" << endl;
	cin >> user;
	unique_lock<mutex> lock(_mt);

	if (_usersConnected.find(user) == _usersConnected.end())
	{
		cout << "No users with this names" << endl;
	}
	else
	{
		_usersConnected.erase(user);
	}

	lock.unlock();

}

void MessagesSender::connectedUsers()
{
	set<string>::iterator usersNames;
	cout << "userNames are:" << endl;
	for (usersNames = _usersConnected.begin(); usersNames != _usersConnected.end(); usersNames++)
	{
		cout << *usersNames << endl; 
	}
}

void MessagesSender::readDataFile()
{
	fstream file;
	string line = "";

	while (true)
	{
		file.open("data.txt");
		if (file.is_open())
		{
			unique_lock <mutex> lock(_mt);
			while (getline(file, line))
			{
				_dataFromFile.push(line);
			}
			lock.unlock();
			_condition.notify_one();
			file.close();
			file.open("data.txt", ifstream::out | ifstream ::trunc);
			file.close();
		}
		this_thread::sleep_for(chrono::seconds(60));
	}

}

void MessagesSender::changeData()
{
	ofstream outFile;
	set<string>::iterator it;
	outFile.open("output.txt");
	while (true)
	{
		if (outFile.is_open())
		{
			unique_lock<mutex> lock(_mt);
			_condition.wait(lock, [&]() {return !_dataFromFile.empty(); });
			if (_usersConnected.empty())
			{
				while (!_dataFromFile.empty()) {

					outFile << _dataFromFile.front() << endl;

					_dataFromFile.pop();
				}
			}
			else 
			{
				while (!_dataFromFile.empty()) {

					for (it = _usersConnected.begin(); it != _usersConnected.end(); it++)
					{
						outFile << *it << ":" << _dataFromFile.front() << endl;

					}
					_dataFromFile.pop();
				}
			}
			lock.unlock();
		}
	}
	outFile.close();
}
