#include "MessagesSender.h"


int main(void)
{
	MessagesSender Example; 

	thread read(&MessagesSender::readDataFile, ref(Example));
	thread mainMenu(&MessagesSender::main, ref(Example));
	thread writeFile(&MessagesSender::changeData, ref(Example));

	writeFile.detach();
	read.detach();
	mainMenu.join();

	system("Pause");
	return 0;


}