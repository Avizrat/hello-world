#include "threads.h"

mutex _mt;
//Check prime numbers and write to the file the prime number
void writePrimesToFile(int begin, int end, ofstream & file)
{
	bool isPrimeNum = true;
	for (int i = begin; i < end; i++)
	{
		for (int j = 2; j < i / 2; j++)
		{
			if (i % j == 0)
			{
				isPrimeNum = false;
				break;
			}
		}
		if (isPrimeNum)
		{
			lock_guard<mutex> lck(_mt);
			file << i << "\n";
		}
		isPrimeNum = true;
	}
}

//Open the file , create a vector for threads  and check tim duration to finish all the threads
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	ofstream file; 
	file.open(filePath);
	vector <thread> threads;
	clock_t StarterClock = clock();

	int newEnd = 0 , diff = int((end - begin) / N);
	for (int i = begin , indexForArray = 0; i < end; i += diff + 1 )
	{
		newEnd = i + diff;
		if (newEnd > end)
		{
			newEnd = end;
		}
		threads.push_back(thread(writePrimesToFile,ref(i), ref(newEnd), ref(file)));
	}
	for (int  j = 0; j < N;j++)
	{
		threads[j].join();
	}
	clock_t durationTime = clock() - StarterClock;
	cout << "Time Duration: " << double(durationTime / CLOCKS_PER_SEC) << endl;
	file.close();
}
