#include <string>
#include <fstream>
#include <iostream>
#include<vector>
#include <thread>
#include <mutex>
using namespace std;

void writePrimesToFile(int begin, int end, ofstream& file);
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N);
