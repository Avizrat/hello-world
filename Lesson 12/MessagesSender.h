#pragma once
#include <iostream>
#include <string>
#include <mutex>
#include <set>
#include <iterator>
#include <fstream>
#include <thread>
#include <queue>

using namespace std;
class MessagesSender
{
public:
	MessagesSender();
	~MessagesSender();
	void main();
	void signIn();
	void signOut();
	void connectedUsers();

	void readDataFile();
	void changeData();

private:
	set<string> _usersConnected;
	mutex _mt;
	queue<string> _dataFromFile;
	condition_variable _condition;
};

