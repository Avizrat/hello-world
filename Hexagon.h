#include "shape.h"
class Hexagon :public Shape
{
public:
	Hexagon(std::string col, std::string nam, double mem);
	~Hexagon();
	virtual void draw();
	virtual double calArea() const;
	void setMember(double member);

private:
	double member;
};
