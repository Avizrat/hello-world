#include <iostream>
#define ERROR 8200

/*addNums 
input - 2 int parameters
output - two nums a and b and return the sum */
int addNums(int a, int b) 
{
	return a + b == ERROR ? ERROR : a + b;
}


/*multiply 
input - 2 int parameters
output - multiply between them by using the function addNums()
*/
int  multiply(int a, int b) {
	int sum = 0;
	for (int i = 0; i < b; i++) {
		sum = addNums(sum, a);
		if (sum == ERROR)
			return ERROR;
	};
	return sum;
}
/*pow -
input - 2 int parameters
output - pow two nums by using the function multiply */
int  pow(int a, int b) {
	int exponent = 1;
	for (int i = 0; i < b; i++) {
		exponent = multiply(exponent, a);
		if (exponent == ERROR)
			return ERROR;
	};
	return exponent;
}

int main(void) {
	int value = 0;
	// Examples for calculator
	//valid:
	if ((value = pow(5, 5)) != ERROR)
	{
		std::cout << value << std::endl;
	}
	else
	{
		std::cout << "The user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year.\n" << std::endl;
	}
	if ((value = multiply(5, 5)) != ERROR)
	{
		std::cout << value << std::endl;
	}
	else
	{
		std::cout << "The user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year.\n" << std::endl;
	}
	if ((value = addNums(5, 5)) != ERROR)
	{
		std::cout << value << std::endl;
	}
	else
	{
		std::cout << "The user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year.\n" << std::endl;
	}
	//invalid
	if ((value = pow(8200, 5)) != ERROR)
	{
		std::cout << value << std::endl;
	}
	else
	{
		std::cout << "The user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year.\n" << std::endl;
	}
	if ((value = multiply(100, 100)) != ERROR)
	{
		std::cout << value << std::endl;
	}
	else
	{
		std::cout << "The user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year.\n" << std::endl;
	}
	if ((value = addNums(8200, 0)) != ERROR)
	{
		std::cout << value << std::endl;
	}
	else
	{
		std::cout << "The user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year.\n" << std::endl;
	}
	system("pause");
}
