#include "Hexagon.h"
#include "shapeexception.h"
#include "MathUtlis.h"


Hexagon::Hexagon(std::string col, std::string nam, double mem):Shape(col,nam)
{
	if (mem < 0)
		throw shapeException();
	setMember(mem);
}


Hexagon::~Hexagon()
{
}

void Hexagon::setMember(double member)
{
	if (member < 0)
		throw shapeException();
	this->member = member;
}

void Hexagon::draw() 
{
	std::cout << getName() << std::endl << getColor() << std::endl << "Area is " << calArea() << std::endl;
}

double Hexagon::calArea() const
{
	return MathUtlis::calHexagonArea(member);
}
