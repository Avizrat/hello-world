#include <iostream>
#include <string>
#define MY_ERROR_CODE -1
#define NOT_ALLOWED_NUMBER 8200

/*addNums 
  input - Two parameters
  output - check if is not error and return the sum if he isn't error it*/
int addNums(int a, int b)
{
	if (a == NOT_ALLOWED_NUMBER || b == NOT_ALLOWED_NUMBER || (a + b) == NOT_ALLOWED_NUMBER) {
		throw(std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year"));
	}
  return a + b;
}
/*multiply
input - Two parameters
output - check if is not error and return the multiply sum by using the function addNums if he isn't error it */
int  multiply(int a, int b)
{
  int sum = 0;
  for(int i = 0; i < b; i++) {
	  if (i == NOT_ALLOWED_NUMBER || sum == NOT_ALLOWED_NUMBER)
	  {
		  throw(std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year"));
	  }
	  try
	  {
		  sum = addNums(sum, a);
	  }
	  catch (const std::string& errorString)
	  {
		  std::cout << "ERROR: " << errorString << std::endl;
		  throw(std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year"));
	  }		
  };
  return sum;
}
/*pow 
  input - Two parameters
  output - check if is not error and return the pow by using multiply if he isn't error it*/
int  pow(int a, int b) 
{
  int exponent = 1;
  for(int i = 0; i < b; i++) {
	  if (i == NOT_ALLOWED_NUMBER || exponent== NOT_ALLOWED_NUMBER)
	  {
		  throw(std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year"));
	  }
	  try
	  {
		  exponent = multiply(exponent, a);
	  }
	  catch (const std::string& errorString)
	  {
		  std::cout << "ERROR: " << errorString << std::endl;
		  throw(std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year"));
	  }
  };
  return exponent;
}

int main(void)
{
	//_____________________________________Check pow function_____________________________________________
	std::cout << "Pow function:" << std::endl;

	try//shouldn't throw
	{
		std::cout << pow(5, 5) << std::endl;
	}
	catch (const std::string& errorString)
	{
		std::cout << "ERROR: " << errorString << std::endl;
	}

	try //should throw 3 times in every function
	{
		std::cout << pow(820, 8500) << std::endl;
	}
	catch (std::string& errorString)
	{
		std::cout << "ERROR: " << errorString << std::endl;
	}

	//_____________________________________Check multiply function_____________________________________________
	std::cout << "multiply function:" << std::endl;
	try//shouldn't throw
	{
		std::cout << multiply(40, 5) << std::endl;
	}
	catch (const std::string& errorString)
	{
		std::cout << "ERROR: " << errorString << std::endl;
	}

	try //should throw 2 times in mul function and add function
	{
		std::cout << multiply(820, 15) << std::endl;
	}
	catch (std::string& errorString)
	{
		std::cout << "ERROR: " << errorString << std::endl;
	}

	//_____________________________________Check add function_____________________________________________
	std::cout << "add function:" << std::endl;
	try//shouldn't throw
	{
		std::cout << addNums(5, 9) << std::endl;
	}
	catch (const std::string& errorString)
	{
		std::cout << "ERROR: " << errorString << std::endl;
	}

	try //should throw
	{
		std::cout << addNums(8200, 5) << std::endl;
	}
	catch (std::string& errorString)
	{
		std::cout << "ERROR: " << errorString << std::endl;
	}


  system("pause");
  return 0;
}