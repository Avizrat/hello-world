#include "MathUtlis.h"
#define COEFFICIENT_HEXAGON 2.59808
#define COEFFICIENT_PENTAGON 1.72048

double MathUtlis::add(double a, double b) {
	return a + b;
}

double MathUtlis::multiply(double a, double b) {
	double sum = 0;
	for (int i = 0; i < b; i++) {
		sum = add(sum, a);
	};
	return sum;
}

double MathUtlis::pow(double a, double b) {
	double exponent = 1;
	for (int i = 0; i < b; i++) {
		exponent = multiply(exponent, a);
	};
	return exponent;
}

double MathUtlis::calPentagonArea(double a)
{
	return multiply(COEFFICIENT_PENTAGON, pow(a, 2));//formula
}

double MathUtlis::calHexagonArea(double a)
{
	return multiply(COEFFICIENT_HEXAGON, pow(a, 2));//formula
}