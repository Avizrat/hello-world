#pragma once
#include "shape.h"
class Pentagon :public Shape
{
public:
	Pentagon(std::string col,std::string nam,double mem);
	~Pentagon();
	virtual void draw();
	virtual double calArea() const;
	void setMember(double member);
private:
	double member;
};

